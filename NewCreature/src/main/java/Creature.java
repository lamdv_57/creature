import java.awt.*;

/**
 * Created by Do Lam on 07/03/2015.
 */
public abstract class Creature implements Runnable{
    protected final Board board;

    protected int y;
    protected int age=0;
    protected int x;
    protected String name;
    private Thread myThread;
    private boolean dead;

    Creature(Board board, int _x, int _y, String name) {
        this.board = board;
        this.x = _x;
        this.y = _y;
        this.name = name;
        this.age = 0;
    }

    public void draw(Graphics g) {
        g.setColor(getColor());
        g.fillOval(x * board.CELL_SIZE, y * board.CELL_SIZE, board.CELL_SIZE, board.CELL_SIZE);
    }

    protected abstract Color getColor();

    public void tick() {
        age++;
        if (age == getMaxAge()){
            System.err.println("dead");
            board.addDead(this);
        }
        if(this.canHaveBaby()){
            System.err.println("canhavebaby");
            this.reproduce();
        }

    }

    abstract protected void reproduce();

    public boolean canHaveBaby() {
        if (age>0&&!dead){
            return true;}
        else return false;
    }


    public boolean atPosition(int _x, int _y) {
        if (x==_x&&y==_y){
            return true;
        }
        return false;
    }



    public void start() {
        myThread = new Thread(this);
        myThread.start();
    }

    @Override
    public void run() {
        while (alive()) {
            try {
                Thread.sleep(getSleepTime());
            } catch (InterruptedException e) {
                break;
            }
            System.err.println("Creature age: " + age);
            tick();
        }
        System.err.println("Creature should be dead now. age: " + age);
    }

    protected abstract long getSleepTime();

    private boolean alive() {
        return !dead && (age < getMaxAge());
    }

    public abstract int getMaxAge();

    public void stop() {
        myThread.interrupt();
    }
}

