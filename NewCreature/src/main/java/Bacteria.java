import java.awt.*;

/**
 * Created by Do Lam on 07/03/2015.
 */
public class Bacteria extends Creature{
    private static final int MAX_AGE = 5;
    private static final int COLOR_STEP = 256/MAX_AGE;

    Bacteria(Board board, int _x, int _y, String bacteria) {
        super(board, _x, _y, bacteria);
    }


    @Override
    protected Color getColor() {
        return new Color(255, COLOR_STEP * age, 255);
    }


    protected void reproduce() {
        if (board.emptyCell(x+1,y+1)){
            Creature newBaby = new Bacteria(board, x+1, y+1, "Bacteria");
            board.addBaby(newBaby);
            newBaby.start();
        }else if (board.emptyCell(x+1,y)){
            Creature newBaby = new Bacteria(board, x+1, y, "Bacteria");
            board.addBaby(newBaby);
            newBaby.start();
        }else if (board.emptyCell(x+1,y-1)){
            Creature newBaby = new Bacteria(board, x+1, y-1, "Bacteria");
            board.addBaby(newBaby);
            newBaby.start();
        }else if (board.emptyCell(x,y-1)){
            Creature newBaby = new Bacteria(board, x, y-1, "Bacteria");
            board.addBaby(newBaby);
            newBaby.start();
        }else if (board.emptyCell(x-1,y-1)){
            Creature newBaby = new Bacteria(board, x-1, y-1, "Bacteria");
            board.addBaby(newBaby);
            newBaby.start();
        }else if (board.emptyCell(x-1,y)){
            Creature newBaby = new Bacteria(board, x-1, y, "Bacteria");
            board.addBaby(newBaby);
            newBaby.start();
        }else if (board.emptyCell(x-1,y+1)){
            Creature newBaby = new Bacteria(board, x-1, y+1, "Bacteria");
            board.addBaby(newBaby);
            newBaby.start();
        }else if (board.emptyCell(x,y+1)){
            Creature newBaby = new Bacteria(board, x, y+1, "Bacteria");
            board.addBaby(newBaby);
            newBaby.start();
        }
    }

    @Override
    protected long getSleepTime() {
        return 3000;
    }

    @Override
    public int getMaxAge() {
        return MAX_AGE;
    }
}