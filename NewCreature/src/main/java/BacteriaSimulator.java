import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.Timer;

/**
 * Created by Do Lam on 07/03/2015.
 */
public class BacteriaSimulator extends JFrame {
    private Board board;
    private JLabel label;
    Timer timer = new Timer(500, new MyTimerActionListener());
    JButton startButton = new JButton("Start");
    JButton stopButton = new JButton("Stop");

    public BacteriaSimulator() {
        initUI();
        setTitle("Bacteria Simulator");
        setSize(350, 200);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    private void initUI() {
        JPanel panel = new JPanel();

        board = new Board();
        panel.add(board);

        label = new JLabel(board.getPopulation());
        panel.add(label);

        startButton.addActionListener(new StartButtonListener());
        panel.add(startButton);
        startButton.setEnabled(true);

        stopButton.addActionListener(new StopButtonListener());
        panel.add(stopButton);
        stopButton.setEnabled(false);

        add(panel);
        pack();
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                BacteriaSimulator ex = new BacteriaSimulator();
                ex.setVisible(true);
            }
        });
    }

    private class StartButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            board.start();
            timer.start();
            startButton.setEnabled(false);
            stopButton.setEnabled(true);
        }
    }

    private class StopButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            board.stop();
            startButton.setEnabled(true);
            stopButton.setEnabled(false);
        }
    }

    private class MyTimerActionListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            label.setText(board.getPopulation());
            repaint();
        }
    }
}