import java.awt.*;

/**
 * Created by Do Lam on 07/03/2015.
 */
public class Bug extends Creature {
    private static final int MAX_AGE = 5;
    private int sub_age = 0;
    private static final int COLOR_STEP = 256/MAX_AGE;

    Bug(Board board, int _x, int _y, String bug) {
        super(board, _x, _y, bug);
    }

    @Override
    protected Color getColor() {
        return new Color(100, COLOR_STEP * age, 100);
    }
    @Override
    public void tick() {
        age++;
        if (age > MAX_AGE){
            board.addDead(this);
        }
        if (this.canHaveBaby()){
            this.reproduce();
        }
    }

    protected void reproduce() {
        if (board.emptyCell(x+1,y+1)){
            Creature newBaby = new Bacteria(board, x+1, y+1, "Bacteria");
            board.addBaby(newBaby);
            newBaby.start();
        }else if (board.emptyCell(x+1,y)){
            Creature newBaby = new Bacteria(board, x+1, y, "Bacteria");
            board.addBaby(newBaby);
            newBaby.start();
        }else if (board.emptyCell(x+1,y-1)){
            Creature newBaby = new Bacteria(board, x+1, y-1, "Bacteria");
            board.addBaby(newBaby);
            newBaby.start();
        }else if (board.emptyCell(x,y-1)){
            Creature newBaby = new Bacteria(board, x, y-1, "Bacteria");
            board.addBaby(newBaby);
            newBaby.start();
        }else if (board.emptyCell(x-1,y-1)){
            Creature newBaby = new Bacteria(board, x-1, y-1, "Bacteria");
            board.addBaby(newBaby);
            newBaby.start();
        }else if (board.emptyCell(x-1,y)){
            Creature newBaby = new Bacteria(board, x-1, y, "Bacteria");
            board.addBaby(newBaby);
            newBaby.start();
        }else if (board.emptyCell(x-1,y+1)){
            Creature newBaby = new Bacteria(board, x-1, y+1, "Bacteria");
            board.addBaby(newBaby);
            newBaby.start();
        }else if (board.emptyCell(x,y+1)){
            Creature newBaby = new Bacteria(board, x, y+1, "Bacteria");
            board.addBaby(newBaby);
            newBaby.start();
        }
    }

    @Override
    protected long getSleepTime() {
        return 1000;
    }

    @Override
    public int getMaxAge() {
        return MAX_AGE;
    }

    @Override
    public void draw(Graphics g) {
        g.setColor(new Color(200, COLOR_STEP * age, 0));
        g.fillOval(x * board.CELL_SIZE, y * board.CELL_SIZE, board.CELL_SIZE, board.CELL_SIZE);
    }

    private void move() {
        if(board.creatureAt(x+1, y) instanceof Bacteria){
            sub_age++;
            if (sub_age==3){
                Creature newBaby = new Bug(board, x+1, y, "Bug");
                board.addDead(board.creatureAt(x+1, y));
                board.addBaby(newBaby);
            }else {
                Creature newBaby = new Bug(board, x+1, y, "Bug");
                board.addDead(board.creatureAt(x, y));
                board.addDead(board.creatureAt(x+1, y));
                board.addBaby(newBaby);
            }
        }else if (board.creatureAt(x, y-1) instanceof Bacteria){
            sub_age++;
            if (sub_age==3){
                Creature newBaby = new Bug(board, x, y-1, "Bug");
                board.addDead(board.creatureAt(x, y-1));
                board.addBaby(newBaby);
            }else {
                Creature newBaby = new Bug(board, x, y-1, "Bug");
                board.addDead(board.creatureAt(x, y));
                board.addDead(board.creatureAt(x, y-1));
                board.addBaby(newBaby);
            }
        }else if (board.creatureAt(x-1, y) instanceof Bacteria){
            sub_age++;
            if (sub_age==3){
                Creature newBaby = new Bug(board, x-1, y, "Bug");
                board.addDead(board.creatureAt(x-1, y));
                board.addDead(newBaby);
            }else {
                Creature newBaby = new Bug(board, x-1, y, "Bug");
                board.addDead(board.creatureAt(x-1, y));
                board.addDead(board.creatureAt(x, y));
                board.addDead(newBaby);
            }
        }else if (board.creatureAt(x, y+1) instanceof Bacteria){
            sub_age++;
            if (sub_age==3){
                Creature newBaby = new Bug(board, x, y+1, "Bug");
                board.addDead(board.creatureAt(x, y+1));
                board.addBaby(newBaby);
            }else {
                Creature newBaby = new Bug(board, x, y+1, "Bug");
                board.addDead(board.creatureAt(x, y));
                board.addDead(board.creatureAt(x, y+1));
                board.addBaby(newBaby);
            }
        }else if (board.creatureAt(x+1, y)== null && board.creatureAt(x, y-1)== null
                && board.creatureAt(x-1, y)== null && board.creatureAt(x, y+1)== null){
            Creature newBaby = new Bug(board, x, y+1, "Bug");
            board.addBaby(newBaby);
        }
    }
}