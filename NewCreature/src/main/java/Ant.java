import java.awt.*;

/**
 * Created by Do Lam on 07/03/2015.
 */
public class Ant extends Creature {
    private static final int MAX_AGE = 5;
    private static final int COLOR_STEP = 256/MAX_AGE;

    Ant(Board board, int _x, int _y, String ant) {
        super(board, _x, _y, ant);
    }

    @Override
    protected Color getColor() {
        return new Color(255, COLOR_STEP * age, 0);
    }

    protected void reproduce() {
        if (board.emptyCell(x+1,y+1)){
            Creature newBaby = new Bacteria(board, x+1, y+1, "Bacteria");
            board.addBaby(newBaby);
            newBaby.start();
        }else if (board.emptyCell(x+1,y)){
            Creature newBaby = new Bacteria(board, x+1, y, "Bacteria");
            board.addBaby(newBaby);
            newBaby.start();
        }else if (board.emptyCell(x+1,y-1)){
            Creature newBaby = new Bacteria(board, x+1, y-1, "Bacteria");
            board.addBaby(newBaby);
            newBaby.start();
        }else if (board.emptyCell(x,y-1)){
            Creature newBaby = new Bacteria(board, x, y-1, "Bacteria");
            board.addBaby(newBaby);
            newBaby.start();
        }else if (board.emptyCell(x-1,y-1)){
            Creature newBaby = new Bacteria(board, x-1, y-1, "Bacteria");
            board.addBaby(newBaby);
            newBaby.start();
        }else if (board.emptyCell(x-1,y)){
            Creature newBaby = new Bacteria(board, x-1, y, "Bacteria");
            board.addBaby(newBaby);
            newBaby.start();
        }else if (board.emptyCell(x-1,y+1)){
            Creature newBaby = new Bacteria(board, x-1, y+1, "Bacteria");
            board.addBaby(newBaby);
            newBaby.start();
        }else if (board.emptyCell(x,y+1)){
            Creature newBaby = new Bacteria(board, x, y+1, "Bacteria");
            board.addBaby(newBaby);
            newBaby.start();
        }
    }

    @Override
    protected long getSleepTime() {
        return 2000;
    }

    @Override
    public int getMaxAge() {
        return MAX_AGE;
    }
}
