import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * Created by Do Lam on 07/03/2015.
 */
public class Board extends JPanel {

    public static final int CELL_SIZE = 10;
    private final int rows = 10;
    private final int cols = 20;
    ArrayList<Creature> creatureList;


    Board() {
        super();
        setBackground(Color.white);
        creatureList = new ArrayList<Creature>();

        setPreferredSize(new Dimension(cols * CELL_SIZE, rows * CELL_SIZE));
        creatureList.add(new Ant(this, 1,1, "Ant"));
        creatureList.add(new Bacteria(this,3,5, "Bacteria"));
        creatureList.add(new Bug(this, 7,7, "Bug"));
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        for (Creature creature: creatureList) {
            creature.draw(g);
        }
    }

    public void addBaby (Creature baby){
        System.err.println("add");
        if (baby != null) synchronized (this){creatureList.add(baby);}
    }

    public void addDead(Creature deadCreature) {
        if (deadCreature != null) synchronized (this){creatureList.remove(deadCreature);}
    }

    public boolean emptyCell(int x, int y) {
        if (x < 0 || y < 0 || x >= cols || y >= rows) return false;
        synchronized (this) {
            for (Creature creature : creatureList) {
                if (creature.atPosition(x, y)) {
                    return false;
                }
            }
        }
        return true;
    }

    public String getPopulation() {
        return String.format("%d", creatureList.size());
    }

    public Creature creatureAt(int x, int y){
        for (Creature creature: creatureList){
            if (creature.atPosition(x, y)){
                return creature;
            }
        }
        return null;
    }

    public void start() {
        creatureList.clear();
        initCreatureList();
        for (Creature creature : creatureList) {
            creature.start();
        }
    }

    private void initCreatureList() {
        creatureList.add(new Bacteria(this,3,5, "Bacteria"));
        creatureList.add(new Ant(this, 1,1, "Ant"));
        creatureList.add(new Bug(this, 7,7, "Bug"));
    }

    public void stop() {
        for (Creature creature: creatureList) {
            creature.stop();
        }
    }
}